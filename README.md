
# Использование Postgres в Docker

* **команда поднятия postgres в docker:**
`docker run --name some-postgres -e POSTGRES_PASSWORD=Support1 -p 5432:5432 -d postgres`

* **остановить все контейнеры docker:**
`docker stop $(docker ps -a -q)`

* **удалить все контейнеры docker:**
`docker rm $(docker ps -a -q)`

* **Зайти на UI Swagger**
`http://localhost:8080/swagger-ui.html`

# Полезная литература

1. [Spring Reactive Guide](https://www.baeldung.com/spring-reactive-guide)
2. [Пара слов про R2DBC и PostgreSQL](https://habr.com/ru/post/500446/)
3. [Reactive Programming and Relational Databases](https://spring.io/blog/2018/12/07/reactive-programming-and-relational-databases)
4. [Spring MVC vs Spring WebFlux. Что лучше? Объясняем на пингвинах](https://habr.com/ru/post/565698/)
5. [Spring WebFlux: Реактивное программирование веб-сервисов](https://habr.com/ru/post/565752/)
6. [Реактивное программирование со Spring](https://habr.com/ru/post/565000/)
7. [Spring Data R2DBC - Reference Documentation](https://docs.spring.io/spring-data/r2dbc/docs/current/reference/html/#preface)
8. [Spring: Blocking vs non-blocking: R2DBC vs JDBC and WebFlux vs Web MVC](https://technology.amis.nl/software-development/performance-and-tuning/spring-blocking-vs-non-blocking-r2dbc-vs-jdbc-and-webflux-vs-web-mvc/)
9. [Манифест Реактивных Систем](https://www.reactivemanifesto.org/ru)
10. [Настроить Swagger 3](https://www.baeldung.com/spring-rest-openapi-documentation)