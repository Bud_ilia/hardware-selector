package com.hardware.selector

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SelectorApplication

fun main(args: Array<String>) {
    runApplication<SelectorApplication>(*args)
}
