package com.hardware.selector.model

import com.fasterxml.jackson.annotation.JsonIgnore
import org.springframework.data.annotation.Id
import org.springframework.data.domain.Persistable
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table
import java.util.*

@Table("processors")
data class Processor(
    val name: String,
    val manufacturer: String,
    val clockSpeed: Int,
    val bitCapacity: Int,
    val socket: String,
    val techProcess: Int,
    val threads: Int,
    val maxRamCapacity: Int,
    val ramType: String,
    val ramSlots: Int,
    val ramFrequency: Int,
    val l1: Int,
    val l2: Int,
    val l3: Int,
    @Id @Column("id") val processorId: UUID = UUID.randomUUID()
) : Persistable<UUID> {

    @JsonIgnore
    override fun getId(): UUID = processorId

    @JsonIgnore
    override fun isNew(): Boolean = true
}
