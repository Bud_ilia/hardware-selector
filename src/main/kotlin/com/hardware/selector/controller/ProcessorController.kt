package com.hardware.selector.controller

import com.hardware.selector.model.Processor
import com.hardware.selector.service.ProcessorService
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.reactive.asFlow
import kotlinx.coroutines.reactive.awaitFirst
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/processors")
class ProcessorController(private val service: ProcessorService) {

    @PostMapping
    suspend fun save(@RequestBody processor: Processor): Processor = service.save(processor).awaitFirst()

    @DeleteMapping("/all/delete")
    fun deleteAll() = service.deleteAll()

    @GetMapping("/all")
    suspend fun getAll(): List<Processor> = service.getAll().asFlow().toList()
}
