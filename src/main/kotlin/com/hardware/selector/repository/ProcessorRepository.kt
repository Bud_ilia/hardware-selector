package com.hardware.selector.repository

import com.hardware.selector.model.Processor
import org.springframework.data.repository.reactive.ReactiveCrudRepository
import java.util.*

interface ProcessorRepository : ReactiveCrudRepository<Processor, UUID>
