package com.hardware.selector.service

import com.hardware.selector.model.Processor
import com.hardware.selector.repository.ProcessorRepository
import org.springframework.stereotype.Service

@Service
class ProcessorService(private val repo: ProcessorRepository) {

    fun save(processor: Processor) = repo.save(processor)

    fun getAll() = repo.findAll()

    fun deleteAll() = repo.deleteAll()
}
