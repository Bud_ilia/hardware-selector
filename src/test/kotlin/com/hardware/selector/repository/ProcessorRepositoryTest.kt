package com.hardware.selector.repository

import com.hardware.selector.model.Processor
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.data.r2dbc.AutoConfigureDataR2dbc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.annotation.DirtiesContext
import java.util.*

@SpringBootTest
@AutoConfigureDataR2dbc
@DirtiesContext
class ProcessorRepositoryTest(
    @Autowired private val repo: ProcessorRepository
) {

    @Test
    fun `save entity`() {
        kotlin.runCatching {
            val entity = repo.save(
                Processor(
                    processorId = UUID.randomUUID(),
                    name = "Intel",
                    bitCapacity = 4,
                    clockSpeed = 3,
                    l1 = 3,
                    l2 = 2,
                    l3 = 23,
                    manufacturer = "44ss",
                    maxRamCapacity = 34,
                    ramFrequency = 44,
                    ramSlots = 33,
                    ramType = "rrr",
                    socket = "ccv",
                    techProcess = 43,
                    threads = 34
                )
            ).toFuture().get()

            val saved = repo.findById(entity.processorId ?: UUID.randomUUID()).toFuture().get()
            println("ENTITY: ${entity.name}")
            Assertions.assertEquals(saved.name, entity.name)
        }
    }
}
