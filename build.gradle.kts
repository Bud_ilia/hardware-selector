import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("org.springframework.boot") version "2.6.2"
    id("io.spring.dependency-management") version "1.0.11.RELEASE"
    kotlin("jvm") version "1.5.31"
    kotlin("plugin.spring") version "1.5.31"
    id("org.jmailen.kotlinter") version "3.8.0"
}

group = "com.hardware"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_11

repositories {
    mavenCentral()
}

dependencies {
    implementation("org.springframework.boot:spring-boot-starter-data-r2dbc:2.6.2")
    implementation("org.springframework.boot:spring-boot-starter-webflux:2.6.2")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin:2.13.1")
    implementation("io.projectreactor.kotlin:reactor-kotlin-extensions:1.1.5")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-reactor:1.6.0-native-mt")
    implementation("org.liquibase:liquibase-core:4.6.2")
    implementation("org.springframework:spring-jdbc:5.3.14")
    runtimeOnly("io.r2dbc:r2dbc-postgresql:0.8.10.RELEASE")
    runtimeOnly("org.postgresql:postgresql:42.3.1")
    testImplementation("org.springframework.boot:spring-boot-starter-test:2.6.2")
    testImplementation("io.projectreactor:reactor-test:3.4.14")

    implementation("org.springdoc:springdoc-openapi-webflux-ui:1.6.3")
    implementation("org.springframework:spring-jdbc:5.3.14")
    testImplementation("org.testcontainers:postgresql:1.16.2")
    testImplementation ("org.testcontainers:testcontainers:1.16.2")
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "11"
    }
}

tasks.withType<Test> {
    useJUnitPlatform()
}
